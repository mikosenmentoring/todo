
var MySchedule = MySchedule || {};

MySchedule.controller = {

    storage : Storage,

    bind : function(){
        MySchedule.view.addPostAction(this.postNewSchedule);
        MySchedule.view.addCheckBoxAction(this.updateCompletely);
        MySchedule.view.addDeleteButtonAction(this.deleteSchedule)
    },

    postNewSchedule : function(id, contents, deadLine){
        var contents = MySchedule.view.getNewContents();
        var deadLine = MySchedule.view.getDeadLine();
        var id = MySchedule.view.getID()
        if(!(contents && deadLine)) return ;
        var schedule = MySchedule.model.schedule.createNewSchedule(id,contents,deadLine,false);
        Storage.putJSON(id,schedule);
        restAPI.todo.post(schedule).then(()=>{
        	MySchedule.view.addListRow(schedule);
        }).catch(e=> {
        	console.log(e);
        })
    },

    deleteSchedule : function(e){
        var id = e.target.id;
        Storage.remove(id);
        MySchedule.view.removeListRow(id);
    },

    getSchedule: function(){
        var id = 0;
        var list = []
        restAPI.todo.get().then((data) => {
        	if(Array.isArray(data)) list = data
        	else list.push(data);
            MySchedule.view.addList(list);
        })
    },

    updateCompletely: function(e, status){
        var id = e.target.id;
        var status = e.target.checked;
        var schedule = Storage.popJSON(id);
        schedule.completely = status;
        Storage.putJSON(id, schedule);
    }
};

$(function () {
    MySchedule.controller.bind();
    MySchedule.controller.getSchedule();
});