
var MySchedule = MySchedule || {};

MySchedule.view = {

    addListRow : function(rowData){
        var tbody = $("#tbody");
        var trs = tbody.find("tr");
        var newRow = $("<tr id='"+rowData.id+"' ></tr>");
        newRow.append("<td>"+rowData.contents+"</td>");
        newRow.append("<td>"+new Date(rowData.deadLine).toDateString()+"</td>");
        newRow.append("" +
            "<td>" +
            "   <label>" +
            "       <input type='checkbox' onchange='MySchedule.controller.updateCompletely(event)'" +
            "              "+(rowData.completely ? "checked" : "")+
            "               value='checkboxA' id='"+rowData.id+"'>" +
            "   </label>" +
            "</td>"
        );
        newRow.append("" +
            "<td>" +
            "   <button class='btn btn-danger btn-xs' id='"+rowData.id +"' onclick='MySchedule.controller.deleteSchedule(event)'>" +
            "       <span id='"+rowData.id +"' class='glyphicon glyphicon-remove'></span>" +
            "   </button>" +
            "</td>"
        );
        tbody.append(newRow);
    },

    addList : function(list){
    	for(let rowData of list){
            var tbody = $("#tbody");
            var trs = tbody.find("tr");
            var newRow = $("<tr id='"+rowData.id+"' ></tr>");
            newRow.append("<td>"+rowData.contents+"</td>");
            newRow.append("<td>"+new Date(rowData.deadLine).toDateString()+"</td>");
            newRow.append("" +
                "<td>" +
                "   <label>" +
                "       <input type='checkbox' onchange='MySchedule.controller.updateCompletely(event)'" +
                "              "+(rowData.completely ? "checked" : "")+
                "               value='checkboxA' id='"+rowData.id+"'>" +
                "   </label>" +
                "</td>"
            );
            newRow.append("" +
                "<td>" +
                "   <button class='btn btn-danger btn-xs' id='"+rowData.id +"' onclick='MySchedule.controller.deleteSchedule(event)'>" +
                "       <span id='"+rowData.id +"' class='glyphicon glyphicon-remove'></span>" +
                "   </button>" +
                "</td>"
            );
            tbody.append(newRow);
    	}
    },

    addPostAction : function(eventHandler){
        $("#submit").on("click",eventHandler);
    },

    addCheckBoxAction : function(eventHandler){
        var elements = $(".complete");
        for(var element of elements){
            $(element).on("click",eventHandler);
        }
    },

    addDeleteButtonAction : function(eventHandler){
        var elements = $(".delete");
        for(var element of elements){
            $(element).on("click",eventHandler);
        }
    },

    removeListRow : function(key) {
        var trs = $("tbody").children("tr");
        var tr = trs.remove("#"+key);
    },

    getDeadLine : function(){
        return $("#Deadline").val();
    },

    getNewContents : function(){
        return $("#Contents").val();
    },

    getID : function () {
        var tbody = $("#tbody");
        var trs = tbody.find("tr");
        if(trs.length === 0) return 0;
        var IDs = trs.map((k,v)=>parseInt(v.id));
        var newID = Math.max.apply(null, IDs)+1;
        return newID
    }
};
