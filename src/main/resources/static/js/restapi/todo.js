
var restAPI = restAPI || {}


restAPI.todo = {

    get : function(key,data,callback){
    	return new Promise((resolve,reject) => {
    		fetch("/todo").then((response) => {
    			if(!response.ok){
    				reject(new Error(`[${response.status}]API Error`))
    			} else {
    				return response.json()
    			}
    		}).then(data => {
    			resolve(data);
    		})
    	})
    },

    post : function(data){
    	return new Promise((resolve,reject) => {
    		fetch("/todo",{
    			method : "POST",
    			headers : {
    				"Content-Type":"application/json; charset=utf-8"
    			},
    			body : JSON.stringify(data)
    		}).then((response) => {
    			if(!response.ok){
    				reject(new Error(`[${response.status}]API Error`))
    			} else {
    				return response.json()
    			}
    		}).then(data => {
    			resolve(data);
    		})
    	})
    },

    remove : function(key){
        storage.removeItem(key);
    }
};