
var storage = window.localStorage;

var Storage = {

    putJSON : function(key,data){
        storage.setItem(key,JSON.stringify(data));
    },

    popJSON : function(key){
        data = JSON.parse(storage.getItem(key));
        if(!data) return null;
        return {
            id : key,
            ...data
        }
    },

    remove : function(key){
        storage.removeItem(key);
    }
};