package com.adaptor.gateway.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import com.entity.model.todo.TodoEntity;


public class MariaDB {
	private final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
	private final String DB_TYPE = "mariadb";
	private final String DB_HOST = "localhost";
	private final String DB_USER = "root";
	private final String DB_PASSWORD = "root";
	private final String DB_NAME = "todo";
	private final String DB_TABLE = "schedule";

	private final String SELECT_ALL = " " +
			"SELECT * FROM " + DB_TABLE;

	public String getURL() {
		StringBuilder sb = new StringBuilder("");
		sb.append("jdbc:");
		sb.append(DB_TYPE);
		sb.append("://");
		sb.append(DB_HOST);
		sb.append("/");
		sb.append(DB_NAME);
		return sb.toString();
	}

	public ArrayList<TodoEntity> getTodo() throws SQLException {
		final ArrayList<TodoEntity> ret = new ArrayList<TodoEntity>();
        Connection conn = null;
		try{
			StringBuilder sb = new StringBuilder("");
            Class.forName (JDBC_DRIVER);

            conn = DriverManager.getConnection(this.getURL(),DB_USER,DB_PASSWORD);
            Statement stmt = conn.createStatement ();
            ResultSet rs = stmt.executeQuery (SELECT_ALL);
            while(rs.next()) {
            	int i = 1;
            	int ID = rs.getInt(i++);
            	String contents = rs.getString(i++);
            	Timestamp deadline = rs.getTimestamp(i++);
            	boolean completed = rs.getBoolean(i++);
            	ret.add(TodoEntity.build(ID, contents, deadline, completed));
            }
       } catch (SQLException e) {
            System.err.println("SQL failed.");
            e.printStackTrace ();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace ();
        } finally {
        	if(conn != null) {
            	conn.close();
        	}
        }
        return ret;
    }

	private final String INSERT_TODO = " " +
			" INSERT INTO " + DB_TABLE +
			" VALUES(?,?,?,?)";

	public ArrayList<TodoEntity> insertTodo(TodoEntity data) throws SQLException {
		final ArrayList<TodoEntity> ret = new ArrayList<TodoEntity>();
        Connection conn = null;
		try{
			StringBuilder sb = new StringBuilder("");
            Class.forName (JDBC_DRIVER);

            conn = DriverManager.getConnection(this.getURL(),DB_USER,DB_PASSWORD);
            PreparedStatement stmt = conn.prepareStatement(INSERT_TODO);
            int i = 1;
            stmt.setInt(i++, data.getId());
            stmt.setString(i++, data.getContents());
            stmt.setTimestamp(i++, Timestamp.from(data.getDeadLine().toInstant()));
            stmt.setBoolean(i++, data.getCompleted());
            stmt.executeUpdate();
		} catch (SQLException e) {
            System.err.println("SQL failed.");
            e.printStackTrace ();
            throw e;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace ();
        } finally {
        	if(conn != null) {
            	conn.close();
        	}
        }
        return ret;
    }

}
