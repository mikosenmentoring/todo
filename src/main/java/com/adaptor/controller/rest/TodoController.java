
package com.adaptor.controller.rest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import com.adaptor.gateway.db.MariaDB;
import com.entity.model.todo.TodoEntity;

public class TodoController {

	public TodoEntity getTodo() {
		return TodoEntity.build(1,"明日も頑張る",new Date(),false);
	}
	public ArrayList<TodoEntity> getTodos() {
        MariaDB db = new MariaDB();
        try {
    		return db.getTodo();
        } catch (SQLException e) {
            System.err.println("SQL failed.");
            e.printStackTrace ();
        }
        return new ArrayList<TodoEntity> ();

	}

	public boolean insertTodo(TodoEntity data) {
        MariaDB db = new MariaDB();
	    try {
	        db.insertTodo(data);
	    } catch (SQLException e) {
	        System.err.println("SQL failed.");
	        e.printStackTrace ();
	        return false;
	    }
	    return true;
	}

}
