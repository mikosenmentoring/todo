package com.adaptor.presenter.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adaptor.controller.rest.TodoController;
import com.entity.model.todo.TodoEntity;

@RestController
@RequestMapping("/todo")
public class Todo {

	@GetMapping
	public ResponseEntity<ArrayList<TodoEntity>> getTodo(@RequestParam("Key") Optional<String> key){
		TodoController todoController = new TodoController();
		ArrayList<TodoEntity> todoEntity = null;
		todoEntity = todoController.getTodos();

		return ResponseEntity.ok(todoEntity);// ←ポイントです！！
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Boolean> postTodo(@RequestBody TodoEntity data){
		TodoController todoController = new TodoController();
		if(todoController.insertTodo(data)) {
			return ResponseEntity.ok(true);
		}else {
			return ResponseEntity.badRequest().body(false);
		}
	}

}
