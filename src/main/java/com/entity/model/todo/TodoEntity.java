package com.entity.model.todo;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TodoEntity implements Serializable {

    private int id;
	private String contents;
	private Date deadLine;
	private boolean complete;

	public TodoEntity() {
    }

	public TodoEntity(int id, String name, Date deadLine) {
    	this.id = id;
    	this.contents = name;
    	this.deadLine = deadLine;
    	this.complete = false;
    }

    public TodoEntity(int id, String name, Date deadLine, boolean complete) {
    	this.id = id;
    	this.contents = name;
    	this.deadLine = deadLine;
    	this.complete = complete;
    }

    public int getId() {
    	return this.id;
    }

    public String getContents() {
    	return this.contents;
    }

    public Date getDeadLine() {
    	return this.deadLine;
    }

    public boolean getCompleted() {
    	return this.complete;
    }

    public String toJSON() throws JsonProcessingException {
    	ObjectMapper m = new ObjectMapper();
    	return m.writeValueAsString(this);
    }
    static public TodoEntity build(int id, String contents, Date deadLine, boolean completed) {
		return new TodoEntity(id,contents,deadLine,completed);
	}
}
