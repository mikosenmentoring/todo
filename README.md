# TodoManager

# JavaScriptの処理の流れ

index.htmlが読み込まれると次の流れで処理をおこなう

* controller.bind関数を実行し、event handlerを登録する

	- 登録ボタンが押されたら、postNewScheduleを実行するようにする
	- 完了チェックボックスがクリックされたら、updateCompletelyを実行するようにする
	- 削除ボタンが押されたら、addDeleteButtonActionを実行するようにする

* controller.getSchedule関数を実行し、現在登録済みのtodoを実行する

	- restAPI.todo.get関数で、サーバーにtodoのデータを問い合わせる。
	- APIのレスポンスが取得できたら、Storage.popJSONを用いてローカルストレージに保存されたtodoデータを取得し、APIのレスポンスとマージする。
	- APIの結果とStorageのデータを合わせてview.addListに渡し、HTMLのテーブルカラムを追加表示する。

登録ボタンが押されると、以下のように処理を行う

* view.addPostActionに登録した関数が実行される

	- viewのgetterから、登録フォームのデータを取得する
	- getIDでは、現在表示中のカラムをカウントし、行数+1を返すようになっている
	- model.schedule.createNewScheduleをを用いてtodoオブジェクトを作成する
	- storage.putJSONを用いて、ローカルストレージに保存する
	- view.addListRowを用いて、HTMLのテーブルカラムを追加する。

	__todo : restAPI.todo.post関数で、サーバーにTodoデータを保存するようにする__

* 削除ボタンが押されると以下のように動作する

	- Storage.removeを用いてローカルストレージからデータを削除する
	- view.removeListRowを用いて表示された行を削除する

	__todo : restAPI.todo.delete関数で、サーバーからTodoデータを削除するようにする__

* 完了ボタンが押されると以下のように動作する

	- Storage.popJSONを用いてローカルストレージから該当行データを取得する
	- completely値をtrueに書き換え、putJSONを実行し、更新する。

	__todo : restAPI.todo.put関数で、サーバーのTodoデータを更新するようにする__
